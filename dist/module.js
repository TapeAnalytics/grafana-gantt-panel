'use strict';

System.register(['app/plugins/sdk', './gantt-panel'], function (_export, _context) {
    "use strict";

    var loadPluginCss, GanttPanelCtrl;
    return {
        setters: [function (_appPluginsSdk) {
            loadPluginCss = _appPluginsSdk.loadPluginCss;
        }, function (_ganttPanel) {
            GanttPanelCtrl = _ganttPanel.GanttPanelCtrl;
        }],
        execute: function () {

            loadPluginCss({
                dark: 'plugins/grafana-gantt-panel/css/gantt-panel-dark.css',
                light: 'plugins/grafana-gantt-panel/css/gantt-panel-light.css'
            });

            _export('PanelCtrl', GanttPanelCtrl);
        }
    };
});
//# sourceMappingURL=module.js.map
