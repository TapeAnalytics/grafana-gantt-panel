import {MetricsPanelCtrl} from 'app/plugins/sdk';
import moment from 'moment';
import d3 from './lib/d3';
import _ from 'lodash';

const has = Object.prototype.hasOwnProperty

const grafanaColors = [
  '#7EB26D',
  '#EAB839',
  '#6ED0E0',
  '#EF843C',
  '#E24D42',
  '#1F78C1',
  '#BA43A9',
  '#705DA0',
  '#508642',
  '#CCA300',
  '#447EBC',
  '#C15C17',
  '#890F02',
  '#0A437C',
  '#6D1F62',
  '#584477',
  '#B7DBAB',
  '#F4D598',
  '#70DBED',
  '#F9BA8F',
  '#F29191',
  '#82B5D8',
  '#E5A8E2',
  '#AEA2E0',
  '#629E51',
  '#E5AC0E',
  '#64B0C8',
  '#E0752D',
  '#BF1B00',
  '#0A50A1',
  '#962D82',
  '#614D93',
  '#9AC48A',
  '#F2C96D',
  '#65C5DB',
  '#F9934E',
  '#EA6460',
  '#5195CE',
  '#D683CE',
  '#806EB7',
  '#3F6833',
  '#967302',
  '#2F575E',
  '#99440A',
  '#58140C',
  '#052B51',
  '#511749',
  '#3F2B5B',
  '#E0F9D7',
  '#FCEACA',
  '#CFFAFF',
  '#F9E2D2',
  '#FCE2DE',
  '#BADFF4',
  '#F9D9F9',
  '#DEDAF7',
]; // copied from public/app/core/utils/colors.ts because of changes in grafana
	// 4.6.0

const panelDefaults = {
    colorMaps: [{text: '', color: '#CCC'}],
    showLegend: true,
    tooltipFields: [],
    durationMultField: 1,
};

export class GanttPanelCtrl extends MetricsPanelCtrl {
  constructor($scope, $injector, $q, $rootScope, $timeout, $window, timeSrv, uiSegmentSrv) {
    super($scope, $injector);
    _.defaultsDeep(this.panel, panelDefaults);
    
    this.panelContainer = null;
    this.timeSrv = timeSrv;
    
    this._colorsPaleteCash = {};
    this._colorsPaleteCash.length = 0;
    this.colorMap = {};
    
    this.events.on('panel-initialized', this.onPanelInitalized.bind(this));
    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.events.on('render', this.onRender.bind(this));
    this.events.on('data-received', this.onDataReceived.bind(this));
    this.events.on('data-error', this.onDataError.bind(this));
    this.events.on('refresh', this.onRefresh.bind(this));
  }
  
  link(scope, elem, attrs, ctrl) {
    ctrl.setContainer(elem.find('.gantt-panel')[0]);
  }
  
  setContainer(container) {
    this.panelContainer = container;
  }

  onInitEditMode() {
    this.addEditorTab(
      'Options',
      'public/plugins/grafana-gantt-panel/editor.options.html',
      1
    );
    
    this.addEditorTab(
      'Colors',
      'public/plugins/grafana-gantt-panel/editor.colors.html',
      1
    );
    
    this.addEditorTab(
      'Legend',
      'public/plugins/grafana-gantt-panel/editor.legend.html',
      1
    );
    
    this.editorTabIndex = 3;
    this.refresh();
  }

  onPanelInitalized(){
    this.updateColorInfo();
    this.onConfigChanged();
    
    this.tooltip = d3.select("body")
					    .append("div")   
					    .attr("class", "tooltip")               
					    .style("opacity", 0);
  }
  
  onRender(){
    this.refresh();
  }
  
  onConfigChanged(update = false) {
    this.refresh();
  }
  
  getPanelWidthBySpan() {
    var trueWidth = 0;
    if (typeof this.panel.span === 'undefined') {
      // get the width based on the scaled container (v5 needs this)
      trueWidth = this.panelContainer.offsetParent.clientWidth;
    } else {
      // v4 and previous used fixed spans
      var viewPortWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      // get the pixels of a span
      var pixelsPerSpan = viewPortWidth / 12;
      // multiply num spans by pixelsPerSpan
      trueWidth = Math.round(this.panel.span * pixelsPerSpan);
    }
    return trueWidth;
  }

  getPanelHeight() {
    // panel can have a fixed height set via "General" tab in panel editor
    var tmpPanelHeight = this.panel.height;
    if ((typeof tmpPanelHeight === 'undefined') || (tmpPanelHeight === "")) {
      // grafana also supplies the height, try to use that if the panel does
		// not have a height
      tmpPanelHeight = String(this.height);
      // v4 and earlier define this height, detect span for pre-v5
      if (typeof this.panel.span != 'undefined') {
        // if there is no header, adjust height to use all space available
        var panelTitleOffset = 20;
        if (this.panel.title !== "") {
          panelTitleOffset = 42;
        }
        tmpPanelHeight = String(this.containerHeight - panelTitleOffset); // offset
																			// for
																			// header
      }
      if (typeof tmpPanelHeight === 'undefined') {
        // height still cannot be determined, get it from the row instead
        tmpPanelHeight = this.row.height;
        if (typeof tmpPanelHeight === 'undefined') {
          // last resort - default to 250px (this should never happen)
          tmpPanelHeight = "250";
        }
      }
    }
    // replace px
    tmpPanelHeight = tmpPanelHeight.replace("px","");
    // convert to numeric value
    var actualHeight = parseInt(tmpPanelHeight);
    return actualHeight;
  }
  
  onDataReceived(dataList){
    var panel = d3.select(this.panelContainer);
    panel.selectAll("*").remove();
    
    if((! this.panel.starttimefield) && (! this.panel.endtimefield)){
		panel.append("p").text("Required: configure starting or end time field (Options).");
		return;
	}
    
    if(dataList.length < 1 || dataList[0].datapoints == null || dataList[0].datapoints.length < 1){
    		panel.append("p").text("No documents.");
    		return;
  	}
    
    if(dataList[0].target != "docs"){
		panel.append("p").text("Panel requires raw documents.");
		return;
	}
    		
    var margin = {
        top : 10,
        right : 30,
        bottom : 10,
        left : (this.panel.yaxiswidth ? this.panel.yaxiswidth : 50)
    };
  
    var height = this.getPanelHeight() - margin.top - margin.bottom - 5;
    var width = this.getPanelWidthBySpan() - margin.right - margin.left - 5;
    
    var docs = dataList[0]["datapoints"];
    this.docs = docs;
    
    var categories = this.getCategories(docs);
    
    var getProperty = function(doc, prop){
        if(! prop)
            return null;
        if(! doc)
            return null;
    
        var keys = prop.split(".");
        
        for (let i = 0; i < keys.length; i++) {
            if(! has.call(doc, keys[i]))
                return null;
                
            doc = doc[keys[i]]
            
            if(! doc)
                return null;
        }
        
        return doc;
    }
    
    var durationToText = function (seconds) {
        var sec_num = Math.floor(seconds);
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    }
    
    var keyFunction = function(doc) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        for (var i = 0; i < 10; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        
        return text;
    };
    
    var starttimefield = this.panel.starttimefield;
    var durationfield = this.panel.durationfield;
    var durationMultField = this.panel.durationMultField;
    var endtimefield = this.panel.endtimefield;
    var groupbyfield = this.panel.groupbyfield;
    
    var rectTransform = function(doc) {
        let starttime = getProperty(doc, starttimefield);
        if(! starttime){
            let endtime = getProperty(doc, endtimefield);
            let duration = getProperty(doc, durationfield) * durationMultField;
            
            starttime = endtime - duration;
        }
        
        let category = getProperty(doc, groupbyfield);
        
        return "translate(" + x(new Date(starttime)) + "," + y(category) + ")";
    };
    
    var rectWidthFunction = function(doc) {
        let starttime = getProperty(doc, starttimefield);
        let endtime = getProperty(doc, endtimefield);
        
        let duration = getProperty(doc, durationfield) * durationMultField;

        if(! starttime)
            starttime = endtime - duration;
        if(! endtime)
            endtime = starttime + duration;
    
        return Math.max(5, (x(new Date(endtime)) - x(new Date(starttime)))); 
    }
    
    var colorfield = this.panel.colorfield;
    var coloropacityfield = this.panel.coloropacityfield;
    var coloropacitymax = this.panel.coloropacitymax ? this.panel.coloropacitymax : 100;
    var coloropacitymin = this.panel.coloropacitymin ? this.panel.coloropacitymin : 0;
    var minopacity = this.panel.minopacity ? this.panel.minopacity : 0.3;
    var inverseopacity = this.panel.inverseopacity;
    var colorMap = this.colorMap;
    var _colorsPaleteCash = this._colorsPaleteCash;
    var rectStyle = function(doc) {
        let color = "#7EB26D";
        let colorValue = getProperty(doc, colorfield);
        if (typeof doc === 'string' || doc instanceof String) {
           colorValue = doc;
           coloropacityfield = null;
        }
        colorValue = colorValue ? colorValue : "";
        if (_.has(colorMap, colorValue)) {
          color = colorMap[colorValue];
        }else if (_colorsPaleteCash[colorValue] === undefined) {
          color = grafanaColors[_colorsPaleteCash.length % grafanaColors.length];
          
          _colorsPaleteCash[colorValue] = color;
          _colorsPaleteCash.length++;
        }else{
          color = _colorsPaleteCash[colorValue];
        }
        
        let opacity = 1;
        if(coloropacityfield){
            let opacityValue = getProperty(doc, coloropacityfield);
            
            if(opacityValue > coloropacitymax){
                opacity = 1;
                if(inverseopacity)
                    opacity = minopacity;
            }else if(opacityValue < coloropacitymin || opacityValue == 0 ){
                opacity = minopacity;
                if(inverseopacity)
                    opacity = 1;
            }else{
                opacityValue = opacityValue - coloropacitymin;
                
                opacity = opacityValue / coloropacitymax;
                
                if(inverseopacity)
                    opacity = 1 - opacity;
            }

            opacity = (1 - minopacity) * opacity + minopacity;
        }
        
        return "fill:" + color + ";" + "fill-opacity:" + opacity + ";"; 
    }
                 
    var tooltipFields = this.panel.tooltipFields;   
    
    var tooltip = this.tooltip;
    var mouseoverFunction = function(doc){
    		if(tooltipFields.length < 1)
    			return;
    		
    		let starttime = getProperty(doc, starttimefield);
            let endtime = getProperty(doc, endtimefield);
            let duration = getProperty(doc, durationfield) * durationMultField;
            if(! starttime)
                starttime = endtime - duration;
            if(! endtime)
                endtime = starttime + duration;
    		
    		var tooltipFieldsHtml = "<table>";
    		
    		for (let i = 0; i < tooltipFields.length; i++) {
    			let key = tooltipFields[i].text;
    			
    			let value = getProperty(doc, key);
    			
    			if(key == '@duration'){
    			    value = durationToText(duration/1000);
    			}else if(key == '@start_time'){
    			    value = moment(new Date(starttime)).format('YYYY-M-D hh:mm:ss');
    			}else if(key == '@end_time'){
    			    value = moment(new Date(endtime)).format('YYYY-M-D hh:mm:ss');
    			}else{
    			    value = JSON.stringify(value, null, 2);
    			}
    			
    			let label = tooltipFields[i].label
    			if(! label) label = key;
    			
    			tooltipFieldsHtml += "<tr>";
    			tooltipFieldsHtml += 	"<th><text>"+label+"</text></th>";
    			tooltipFieldsHtml += 	"<th><text>"+value+"</text></th>";
    			tooltipFieldsHtml += "</tr>";
    		}
    		
    		tooltipFieldsHtml += "</table>";
    	      
    		tooltip.html(tooltipFieldsHtml)  
                .style("left", (d3.event.pageX) + "px")     
                .style("top", (d3.event.pageY - 28) + "px");
        
    		tooltip.style("opacity", 1);
    		
    		tooltip.selectAll("text")
                .style("color", 'white');
    }
    
    var legendHeight = 30;
    if(! this.panel.showLegend)
        legendHeight = 0;
    
    var timeDomainStart = this.range.from._d.getTime();
    var timeDomainEnd = this.range.to._d.getTime();
    var x = d3.scaleTime().domain([ timeDomainStart, timeDomainEnd ]).range([ 0, width ]).clamp(true);
    var y = d3.scaleBand().domain(categories).range([0, height - margin.top - margin.bottom - legendHeight]);
  
    var svg = panel.append("svg")
                .attr("class", "chart")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .append("g")
                    .attr("class", "gantt-chart")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom - legendHeight)
                    .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");
                        
    svg.selectAll(".chart")
                 .data(docs, keyFunction)
                 .enter()
                 .append("rect")
                 .attr("rx", 5)
                 .attr("ry", 5)
                 .attr("style", rectStyle) 
                 .attr("y", 0)
                 .attr("transform", rectTransform)
                 .attr("height", function(doc) { return y.bandwidth() - 2; })
                 .attr("width", rectWidthFunction)
                 .on("mouseover", mouseoverFunction)   
                 .on("mousemove", function(){ 
                        tooltip
                            .style("top", (d3.event.pageY) + "px")
                            .style("left", (Math.min(d3.event.pageX, 700)) +"px"); 
                 })
                 .on("mouseout", function() { tooltip.style("opacity", 0); });
                 
    var xAxis = d3.axisBottom(x)
                 .tickFormat(d3.timeFormat("%H:%M"))
                 .tickSize(8)
                 .tickPadding(8);
    svg.append("g")
                 .attr("class", "x axis")
                 .attr("transform", "translate(0, " + (height - margin.top - margin.bottom - legendHeight) + ")")
                 .transition()
                 .call(xAxis);
     
    var yAxis = d3.axisLeft(y)
                 .tickSize(0);
    svg.append("g")
                 .attr("class", "y axis")
                 .transition()
                 .call(yAxis);
    
    if(this.panel.showLegend){             
        var colorValues = this.getColorValues()       

        var radius = Math.min(width, legendHeight) / 2;
        var donutWidth = 75;
        var legendRectSize = 18;                                  
        var legendSpacing = 4;   
    
        var prevXends = 0;
    
        panel.append("svg")
                    .attr("class", "legend")
                    .attr("width", width - margin.left - margin.right)
                    .attr("height", legendHeight);
        var legend = svg.selectAll('.legend')
                          .data(Array.from(colorValues).sort())                 
                          .enter()                           
                          .append('g')                                            
                          .attr('class', 'legendLabel')                                
                          .attr('transform', function(d, i) {
                                var x = prevXends;
                                var y = height - margin.top - margin.bottom + 5;
                                
                                var tlength = (d && d.length) ? d.length : 3;
                                
                                prevXends = prevXends + legendRectSize + legendSpacing + (tlength * 12);
                                                       
                                return 'translate(' + x + ',' + y + ')';        
                          });                                                     
        legend.append('rect')                                     
                      .attr('width', legendRectSize)                          
                      .attr('height', legendRectSize)                         
                      .attr('style', rectStyle)      
				      .attr("rx", 5)
				      .attr("ry", 5);
        legend.append('text')                                     
                      .attr('x', legendRectSize + legendSpacing)              
                      .attr('y', legendRectSize - legendSpacing)              
                      .text(function(d) {
                    	  		if(d)
                    	  			return d;
                    	  		else
                    	  			return "null"
                    	  	});
     }
  }
  
  getEarliestTime(docs){
    let time = Number.MAX_SAFE_INTEGER;
  
    for (let i = 0; i < docs.length; i++) {
        var doc = docs[i];
        
        var docTime = this.getProperty(doc, this.panel.starttimefield);
        if(docTime && time > docTime)
            time = docTime;
            
        var docTime = this.getProperty(doc, this.panel.endtimefield);
        if(docTime && time > docTime)
            time = docTime;
    }
    
    return new Date(time);
  }
  
  getLatestTime(docs){
    let time = Number.MIN_SAFE_INTEGER;
  
    for (let i = 0; i < docs.length; i++) {
        var doc = docs[i];
        
        var docTime = this.getProperty(doc, this.panel.starttimefield);
        if(docTime && time < docTime)
            time = docTime;
            
        var docTime = this.getProperty(doc, this.panel.endtimefield);
        if(docTime && time < docTime)
            time = docTime;
    }
    
    return new Date(time);
  }
  
  getCategories(docs){
    let categories = [];
  
    for (let i = 0; i < docs.length; i++) {
        var doc = docs[i];
        
        var category = this.getProperty(doc, this.panel.groupbyfield);
        
        categories.push(category);
    }
    
    categories.sort();
    
    return categories;
  }

  getProperty(doc, prop){
    if(! prop)
        return null;
            
    var keys = prop.split(".");
    
    for (let i = 0; i < keys.length; i++) {
        doc = doc[keys[i]]
        
        if(! doc)
            return null;
    }
    
    return doc;
  }

  onDataError(err){
    this.seriesList = [];
    console.log('onDataError', err);
  }
  
  onRefresh(){
    // ignore fetching data if another panel is in fullscreen
    if (this.otherPanelInFullscreenMode()) {
      return;
    }
     
  }
  
  removeColorMap(map) {
    let index = _.indexOf(this.panel.colorMaps, map);
    this.panel.colorMaps.splice(index, 1);
    this.updateColorInfo();
  }
  
  updateColorInfo() {
    let cm = {};
    for (let i = 0; i < this.panel.colorMaps.length; i++) {
      let m = this.panel.colorMaps[i];
      cm[m.text] = m.color;
    }
    this._colorsPaleteCash = {};
    this._colorsPaleteCash.length = 0;
    this.colorMap = cm;
    this.render();
  }
  
  addColorMap(what) {
    if (what === 'curent') {
      let colorValues = this.getColorValues();
      
      for (let colorValue of colorValues){
        let v = {text: colorValue, color: this.getColor(colorValue)};
        this.panel.colorMaps.push(v);
        this.colorMap[colorValue] = v;
      }
    } else {
      this.panel.colorMaps.push({text: '???', color: this.randomColor()});
    }
    this.updateColorInfo();
  }
  
  getColorValues(){
    let values = new Set([]);
  
    for (let i = 0; i < this.docs.length; i++) {
        var doc = this.docs[i];
        
        var value = this.getProperty(doc, this.panel.colorfield);
        
        values.add(value);
    }
    
    return values;
  }
  
  getColor(val) {
    if (_.has(this.colorMap, val)) {
      return this.colorMap[val];
    }
    if (this._colorsPaleteCash[val] === undefined) {
      let c = grafanaColors[this._colorsPaleteCash.length % grafanaColors.length];
      this._colorsPaleteCash[val] = c;
      this._colorsPaleteCash.length++;
    }
    return this._colorsPaleteCash[val];
  }
  
  randomColor() {
    let letters = 'ABCDE'.split('');
    let color = '#';
    for (let i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * letters.length)];
    }
    return color;
  }
  
  removeTooltipField(what) {

  }
  
  addTooltipField(what) {
    this.panel.tooltipFields.push({text: '', label: ''});
    
    this.updateTooltipFields();
  }
  
  updateTooltipFields(){
    this.refresh();
  }
  
  removeTooltipField(what) {
    let index = _.indexOf(this.panel.tooltipFields, what);
    this.panel.tooltipFields.splice(index, 1);
    
    this.updateTooltipFields();
  }
  
}

GanttPanelCtrl.templateUrl = 'module.html';
